import requests
from lxml.html import fromstring
import csv
import os
from datetime import date

price_ranges = {
    1: [0, 500000],
    2: [500001, 600000],
    3: [600001, 700000],
    4: [700001, 800000],
    5: [800001, 900000],
    6: [900001, 1000000],
    7: [1000001, 1200000],
    8: [1200001, 1400000],
    9: [1400001, 1600000],
    10: [1600001, 9900000],
}

cities = {'krakow': [0, 5, 10, 15, 30], 
          'niepolomice': [0, 2, 5, 10], 
          'zabierzow_110875': [0, 2, 5, 10], 
          'wieliczka': [0, 2, 5, 10], 
          'batowice': [0, 2, 5]}

def olx_url_builder(real_estate_type, city, dist=None, lo_price=None, hi_price=None):
    base_url = (f"https://www.olx.pl/nieruchomosci/{real_estate_type}/{city}/")
    extension_url = ''
    if dist:
        extension_url += f"?search%5Bdist%5D={dist}"
    if lo_price:
        extension_url += f"&search%5Bfilter_float_price:from%5D={lo_price}"
    if hi_price:
        extension_url += f"&search%5Bfilter_float_price:to%5D={hi_price}"
    return base_url + extension_url 

def get_number_of_ads_from_html(html_tree):
    xpath = "//ul[@data-testid='category-count-links']/li/a[contains(.,'Sprzeda')]/span"
    try:
        return html_tree.xpath(xpath)[0].text
    except:
        return 0

def get_olx_data(real_estate_type, city, dist=None, price_range=None):  
    url = olx_url_builder(real_estate_type, city, dist=dist, lo_price=price_range[0], hi_price=price_range[1])

    s = requests.Session()
    s.proxies = {
    'http': '10.144.1.10:8080',
    'https': '10.144.1.10:8080',
    }

    page = s.get(url, stream=True)
    tree = fromstring(page.text)
    sell_count = get_number_of_ads_from_html(tree)
    save_data_in_csv('olx', real_estate_type, 'sell', city, dist, price_range, str(date.today()), sell_count)

def initialize_csv_file(csv_path):
    if not os.path.isfile(csv_path):
        header = ['webpage', 'real estate type', 'transaction_type', 'city', 'distance', 'price_range', 'date', 'number of ads']
        with open(csv_path, 'w', newline='') as f:    
            writer = csv.writer(f)
            writer.writerow(header)             
        
def save_data_in_csv(*args):
    with open('csv_file.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([*args])           


if __name__ == "__main__":
    initialize_csv_file('csv_file.csv')
    for city, ranges in cities.items():
        for dist in ranges:
            for id, price_range,  in price_ranges.items():
                get_olx_data('domy', city, dist, price_range)